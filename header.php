<?php 
/**
* header.php
*
* The header of the theme.
* Package mid Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright (c) 2019, MI Dexigner (TM)
* Link http://www.midexigner.com
*/
 ?>
 <!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		
<?php if (is_search()) { ?>
  <meta name="robots" content="noindex, nofollow">
<?php } ?>
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">

		<link rel="profile" href="http://gmpg.org/xfn/11">
			<?php if( is_singular() && pings_open( get_queried_object() ) ): ?>
			<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
			<?php endif; ?>
		

		<!-- [if it IE 9]
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> 
		<![endif]-->

	<?php wp_head(); ?>
	</head>


	<body <?php body_class(); ?>>
<?php do_action( 'main_wrapper_start'); ?>
<?php mi_loader(); ?>
<?php if(!is_page_template('template-landing.php')): ?>
<!-- // being top bar -->
<?php if(is_active_sidebar('topbar-left-column')):  ?>
<div id="top-bar">
	<div class="container">
		<div class="row">
			<?php get_sidebar('topbar'); ?>
		</div>
	</div>
</div>
<?php endif; ?>
<?php endif; ?>
<!-- // end top bar -->
	<!-- header -->
<header class="header-sticky site-header"  <?php echo (header_image() != '' ? 'data-imgurl="header_image()"' : ''); ?>>
<nav class="navbar   navbar-light navbar navbar-expand-md">
		<div class="container header-contents">
			
        <div class="site-logo">
        <?php get_template_part( 'template-parts/header/content', 'branding' ); ?>
		
		</div>
					<!-- end of site-logo -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="site-navigation ml-md-auto" role="navigation">
         <?php mi_main_menu(); ?>
	</div>
		</div>
	<!-- end of container -->
	</nav><!-- end of nav -->
	</header>
	<!-- end site-header -->


	

