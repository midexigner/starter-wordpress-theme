jQuery(document).ready(function($){

var mediaUploader;

$('#upload-button').on('click',function(e){
	e.preventDefault();

	if(mediaUploader){
		mediaUploader.open();
		return;
	}

// Create a new media frame
mediaUploader = wp.media.frames.file_frame = wp.media({
title: 'Choose a Logo',
      button: {
        text: 'Choose Logo'
      },
      multiple: false  // Set to true to allow multiple files to be selected	
});

// Get media attachment details from the frame state
mediaUploader.on('select',function(){
	attachment = mediaUploader.state().get('selection').first().toJSON();
	// Send the attachment id to our hidden input
	$('#add-logo').val(attachment.url);
	$('#add-logo-preview').css('background-image','url('+attachment.url+')');
});

mediaUploader.open();

});

$('#remove-logo').on('click',function(e){
	e.preventDefault();
	var answer = confirm('Are You want to remove your Logo ?');
	if(answer == true){
	$('#add-logo').val('');
	$('.mi-general-form').submit();
	console.log('Yes, Please delete!');
	}else{
		console.log('No, Please don\'t!');
	}
	return;
});

});