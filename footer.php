<?php 
/**
* footer.php
*
* The template for displaying the footer.
* Package mid Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright (c) 2019, MI Dexigner (TM)
* Link http://www.midexigner.com
**/
 ?>

<?php if(is_active_sidebar('footer-one')):  ?>
<!-- footer -->
<footer class="siter-footer">
	<div class="container">
		<div class="row">
		<?php get_sidebar('footer'); ?>
		</div>
	</div>
	<!-- end container -->
</footer>
<!-- end site-footer -->
<?php endif; ?>

	<div class="copyright">
			<div class="container">
				<div class="row">
					 <?php if(is_active_sidebar('footer-bottom-left-column')):  ?>
					<?php get_sidebar('bottombar'); ?>
					<?php else: ?>
					<div class="col">
						<p>
				&copy; <?php echo date('Y'); ?>
				<a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>
				<?php _e('. All right reserved','mid'); ?>
			</p>
					</div>
				<?php endif; ?>
				</div>
			</div>
		</div>
		<!-- end copyright -->

<?php wp_footer(); ?>