require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
project_type = :stand_alone
http_path = "/"
sass_dir = "sass"
css_dir = "assets/css"
images_dir = "assets/img"
fonts_dir = 'assets/fonts/'
javascripts_dir = "assets/js"
line_comments = false
preferred_syntax = :scss
output_style = :compressed
relative_assets = true
environment = :development
color_output = true