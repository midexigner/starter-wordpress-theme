<?php 
/**
 * template-contact.php
 *
 * Template Name: Contact Page
 * Package mid Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright (c) 2019, MI Dexigner (TM)
* Link http://www.midexigner.com
 */

?>

 <?php get_header(); ?>

<?php inner_page_banner();?>

<!-- start content -->
<div id="primary" class="content-area">

<main id="main" class="site-main">
	<div class="container">
<div class="row">
	<div class="col-md-4">
		<article>
		<ul class="list-inline">
			<li> <i class="fas fa-map-marker-alt"></i> <?php echo esc_attr( get_option( 'address' ) ); ?></li>
			<li><a href="tel:<?php echo esc_attr( get_option( 'phone_number' ) ); ?>"><i class="fa fa-phone"></i> <?php echo esc_attr( get_option( 'phone_number' ) ); ?></a></li>
			<li><a href="mailto:<?php echo esc_attr( get_option( 'email_address' ) ); ?>"><i class="fa fa-envelope"></i> <?php echo esc_attr( get_option( 'email_address' ) ); ?></a></li>
			<?php 
			if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) { ?>	
	<li> <a href="https://<?php echo esc_attr( get_option( 'website' ) ); ?>" target="_blank"><i class="fa fa-globe"></i> <?php echo esc_attr( get_option( 'website' ) ); ?></a> </li>
			<?php }else{ ?>
			<li> <a href="http://<?php echo esc_attr( get_option( 'website' ) ); ?>" target="_blank"><i class="fa fa-globe"></i> <?php echo esc_attr( get_option( 'website' ) ); ?></a> </li>
		<?php } ?>
			
		</ul>
		</article>
	</div>
 <div class="main-content col-md-8">
<?php while( have_posts() ) : the_post(); ?>
	<?php get_template_part( 'template-parts/page/content', 'contact' ); ?>		
		<?php endwhile; ?>
	</div> <!-- end main-content -->
</div>
</div>
</main>
</div>

<?php get_footer(); ?>