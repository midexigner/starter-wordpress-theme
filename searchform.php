<?php 
/**
* searchform.php
*
* The template for displaying search results.
* Package mid Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright (c) 2019, MI Dexigner (TM)
* Link http://www.midexigner.com
**/
 ?>
<div class="widget">
 <form method="get" id="searchform" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
 	<label>
		<h5 class="widget-title sr-only"><?php _e( 'Search', 'mid' ); ?></h5>
		<input type="search" class="search-field" name="s" id="s" placeholder="<?php esc_attr_e( 'Search', 'mid' ); ?>" />
		</label>
		<button type="submit " class="search-submit" name="submit" id="searchsubmit"><i class="fa fa-search"></i></button>
	</form>
</div>
