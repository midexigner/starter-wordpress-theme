<?php
/**
 * functions.php
 *
 * The theme's functions and definitions.
 */

/**
 * ----------------------------------------------------------------------------------------
 * 1.0 - Define constants.
 * ----------------------------------------------------------------------------------------
 */
define( 'THEMEROOT', get_stylesheet_directory_uri() );
define( 'ROOT', THEMEROOT . '/assets/' );
define( 'STYLES', THEMEROOT . '/assets/css/');
define( 'IMAGES', THEMEROOT . '/assets/img/' );
define( 'SCRIPTS', THEMEROOT . '/assets/js/' );
define('FRAMEWORK', get_template_directory(). '/inc/');
define('THEMENAME',wp_get_theme());

require_once FRAMEWORK . 'init.php' ;
require_once FRAMEWORK . 'vendor/Mobile_Detect.php';
require_once FRAMEWORK . 'cleanup.php';
require_once FRAMEWORK . 'function-admin.php';
require_once FRAMEWORK . 'enqueue.php';
require_once FRAMEWORK . 'theme-support.php';
require_once FRAMEWORK . 'message-post-type.php';
require_once FRAMEWORK . 'walker.php';
require_once FRAMEWORK . 'class-bootstrap-four-navwalker.php';
require_once FRAMEWORK . 'class-tgm-plugin-activation.php';
require_once FRAMEWORK . 'register-plugins.php';
require_once FRAMEWORK . 'schema.php';
require_once FRAMEWORK . 'ajax.php';
?>