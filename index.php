<?php
/**
* index.php
*
* The main template file
* Package mid Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright (c) 2019, MI Dexigner (TM)
* Link http://www.midexigner.com
*/
?>
<?php get_header(); ?>

   
       
<?php mi_featured_blog();?>
<!-- start content -->
<div id="primary" class="content-area">

<main id="main" class="site-main">
	<div class="container">
<div class="row">
 <div class="main-content col-md-9 ">
  <div class="mi-posts-container">
<?php  if (have_posts()) : 
echo '<div class="page-limit" data-page="/' . mi_check_paged() . '">';
  while (have_posts()) :the_post();
    /*
              $class = 'reveal';
              set_query_var( 'post-class', $class );
              */
      get_template_part('template-parts/post/content',get_post_format()); ?>
            <?php  endwhile;
            echo '</div>'; ?>
            <!-- post navigation -->
	<?php //mi_paging_nav() ?>
            <!-- end of post navigation -->
              <?php  else: ?>
               <?php  get_template_part('template-parts/post/content','none'); ?>
                      <?php endif; ?>
</div><!-- /.mi-posts-container -->
   <div class="load-more">
  <div class=" container">
    <div class="text-center">
      <a class="btn-mi-load mi-load-more" data-page="1" data-url="<?php echo admin_url('admin-ajax.php');?>">
<span class="icofont-spinner mi-loading"></span>
      <span class="text">Load More</span></a>
    </div><!-- /.text-center -->
  </div><!-- /.col-md-9 -->
</div><!-- /.load-more -->
</div><!-- /.container -->
 

<!-- end of content -->
<?php if ( is_singular() ): wp_enqueue_script( "comment-reply" ); ?>
<?php comments_template(); ?>
<?php endif; ?>

<!-- start sidebar -->

<?php get_sidebar(); ?>

<!-- end of sidebar -->
</div><!-- .row -->

</div><!-- .container -->

</main>	
</div><!-- #primary -->


<!-- start footer -->

<?php get_footer(); ?>

<!-- end of footer -->



