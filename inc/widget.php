<?php 
//enable rendering shortcodes in widgets
add_filter('widget_text', 'do_shortcode');

$widget = get_option( 'widget_enabled' );
if( @$widget == 1 ){
if ( ! function_exists( 'mi_widget_init' ) ) {
	function mi_widget_init() {
		if ( function_exists( 'register_sidebar' ) ) {
			register_sidebar(
				array(
					'name' => __( 'Topbar Left Column', 'mid' ),
					'id' => 'topbar-left-column',
					'description' => __( 'Appears on posts and pages.', 'mid' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget' => '</div> <!-- end widget -->',
					'before_title' => '<h5 class="widget-title">',
					'after_title' => '</h5>',
				)
			);
			register_sidebar(
				array(
					'name' => __( 'Top Middle Column', 'mid' ),
					'id' => 'topbar-middle-column',
					'description' => __( 'Appears on posts and pages.', 'mid' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget' => '</div> <!-- end widget -->',
					'before_title' => '<h5 class="widget-title">',
					'after_title' => '</h5>',
				)
			);
			register_sidebar(
				array(
					'name' => __( 'Top Right Column', 'mid' ),
					'id' => 'topbar-right-column',
					'description' => __( 'Appears on posts and pages.', 'mid' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget' => '</div> <!-- end widget -->',
					'before_title' => '<h5 class="widget-title">',
					'after_title' => '</h5>',
				)
			);
			register_sidebar(
				array(
					'name' => __( 'Sidebar', 'mid' ),
					'id' => 'sidebar',
					'description' => __( 'Appears on posts and pages.', 'mid' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget' => '</div> <!-- end widget -->',
					'before_title' => '<h5 class="widget-title">',
					'after_title' => '</h5>',
				)
			);

			register_sidebar(
				array(
					'name' => __( 'Footer Column One', 'mid' ),
					'id' => 'footer-one',
					'description' => __( 'Appears on the footer.', 'mid' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget' => '</div> <!-- end widget -->',
					'before_title' => '<h5 class="widget-title">',
					'after_title' => '</h5>',
				)
			);
			register_sidebar(
				array(
					'name' => __( 'Footer Column Two', 'mid' ),
					'id' => 'footer-two',
					'description' => __( 'Appears on the footer.', 'mid' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget' => '</div> <!-- end widget -->',
					'before_title' => '<h5 class="widget-title">',
					'after_title' => '</h5>',
				)
			);
			register_sidebar(
				array(
					'name' => __( 'Footer Column Three', 'mid' ),
					'id' => 'footer-three',
					'description' => __( 'Appears on the footer.', 'mid' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget' => '</div> <!-- end widget -->',
					'before_title' => '<h5 class="widget-title">',
					'after_title' => '</h5>',
				)
			);
			register_sidebar(
				array(
					'name' => __( 'Footer Bottom Left Column', 'mid' ),
					'id' => 'footer-bottom-left-column',
					'description' => __( 'Appears on posts and pages.', 'mid' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget' => '</div> <!-- end widget -->',
					'before_title' => '<h5 class="widget-title">',
					'after_title' => '</h5>',
				)
			);
			register_sidebar(
				array(
					'name' => __( 'Footer Bottom Middle Column', 'mid' ),
					'id' => 'footer-bottom-middle-column',
					'description' => __( 'Appears on posts and pages.', 'mid' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget' => '</div> <!-- end widget -->',
					'before_title' => '<h5 class="widget-title">',
					'after_title' => '</h5>',
				)
			);
			register_sidebar(
				array(
					'name' => __( 'Footer Bottom Right Column', 'mid' ),
					'id' => 'footer-bottom-right-column',
					'description' => __( 'Appears on posts and pages.', 'mid' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget' => '</div> <!-- end widget -->',
					'before_title' => '<h5 class="widget-title">',
					'after_title' => '</h5>',
				)
			);
		}
	}

	add_action( 'widgets_init', 'mi_widget_init' );
}
}




 ?>