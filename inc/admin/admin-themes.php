<?php if (!function_exists('mi_info_page')) {
	function mi_info_page() {
	$page1=add_theme_page(__('Welcome to MI Dexigner', 'mid'), __('About MI Dexigner', 'mid'), 'edit_theme_options', 'mi_welcome_create_page', 'mi_welcome_info_page');
	
	add_action('admin_print_styles-'.$page1, 'mi_admin_info');
	}	
}
add_action('admin_menu', 'mi_info_page');

function mi_admin_info(){
	// CSS
	wp_enqueue_style('bootstrap',  get_stylesheet_directory_uri() .'/assets/admin/plugin.css');
	//JS
	wp_enqueue_script('jquery');
	wp_enqueue_script('bootstrap-js',get_template_directory_uri() .'/assets/admin/plugins.js');
} 
if (!function_exists('mi_welcome_info_page')) {
	function mi_welcome_info_page() {
		$theme_data = wp_get_theme(); ?>
	<div class="wrap mi-page-welcome about-wraper setting-page">

    <div class="col-md-12 settings">
    	<div class="row">
         <div class=" col-md-9">
            <div class="col-md-12">
				<?php $themename = wp_get_theme(); ?>
				<h2>Welcome to <?php echo $themename; ?> - Version <?php echo esc_html( $themename->get('Version') ); ?></h2>
				<p style="font-size:19px;color: #555d66;"><?php echo $themename; ?>  is an outstanding superfine creation from MI Dexigner , <?php echo $themename; ?> has become one of the most popular superfine multipurpose responsive theme with WPMLlovers by many professionals for its amazing capabilities and great designs. <?php echo $themename; ?> is multi function free WordPress theme with responsive and flexible light-weighted WooCommerce theme built by using core WordPress functions (BOOTSTRAP CSS framework) that works on All leading web browsers weather its mobile device or any other this makes it fast, simple and easy to customize! It has a promising design and powerful features with page layouts and sidebar widget that let your websites easy to design. </p>
            </div>
			
		</div>
       
        <div class=" col-md-3">
			<div class="update_pro">
				<h3> Upgrade Pro </h3>
				<div class="alert alert-danger">
					<h3>Coming Soon</h3>
				</div>
				<!-- <a class="demo" href="#">Get Pro In $$$$</a> -->
				
			</div>
		</div>
		</div>
	</div>

            <!-- Themes & Plugin -->
            <div class="col-md-12  product-main-cont">
                <ul class="nav nav-pills nav-fill nav-tabs">
				    <li  class="nav-item"><a data-toggle="tab" href="#start" class="nav-link active"> Getting Started </a></li>
                    <li class="nav-item"><a data-toggle="tab" href="#themesd" class="nav-link"> <?php echo $themename; ?> premium </a></li>
					<li class="nav-item"><a data-toggle="tab" href="#freepro" class="nav-link">Free Vs Pro</a></li> 
					
                </ul>

                <div class="tab-content" style="margin-top: 4em;">
				
				
				<div id="start" class="tab-pane fade show tab-pane active">
                        <div class="space  theme active">

                            <div class=" p_head theme">
                                <!--<h1 class="section-title">WordPress Themes</h1>-->
                            </div>							

                            <div class="row p_plugin blog_gallery">
                                <div class="col-xs-12 col-sm-7 col-md-7 p_plugin_pic">
                                    <h4>Step 1: Create a Homepage</h4>
									<ol>
									<li> Create a new page -> home and publish. </li>
									<li> Go to Settings -> Reading -> select A static page option. </li>
									<li> Save changes </li>
									</ol>
									<a class="add_page btn btn-primary" target="_blank" href="<?php echo admin_url('/post-new.php?post_type=page') ?>">Add New Page</a>
                                </div>
                                <div class="col-xs-12 col-sm-5 col-md-5 p_plugin_desc">
                                    <div class="row p-box">
                                         <div class="img-thumbnail">
										<img src="<?php echo get_template_directory_uri(); ?>/screenshot.png" class="img-fluid" alt="img"/>
                                    </div>
                                    </div>
                                </div>
                            </div>
							
							
							
							
							<div class="row p_plugin blog_gallery visit_pro">
                                <h4>Visit Our Latest Pro Themes & See Demos</h4>
								<p style="font-size: 17px !important;">We have put in a lot of effort in all our themes, free and premium both. Each of our Premium themes has a corresponding free version so that you can try out the theme before you decide to purchase it.</p>
								<a href="#">Visit Themes</a>
                            </div>	
                        </div>
                    </div>
				
				<!-- end 1st tab -->
				
				
                    <div id="themesd" class="tab-pane fade">
                        <div class="space theme">

                            <div class=" p_head theme">
                                <!--<h1 class="section-title">WordPress Themes</h1>-->
                            </div>							

                            <div class="row p_plugin blog_gallery">
                                <div class="col-xs-12 col-sm-4 col-md-5 p_plugin_pic">
                                    <div class="img-thumbnail">
										<img src="<?php echo get_template_directory_uri(); ?>/screenshot.png" class="img-fluid" alt="img"/>
                                    </div>
									
                                </div>
                                <div class="col-xs-12 col-sm-5 col-md-5 p_plugin_desc">
                                    <div class="row p-box">
                                        <div>
                                        	
                                            <p><strong>Description: </strong>MID is an elegant, versatile theme that gives you the tools you need to express who you are and what your business does in a professional and coherent manner. To help you get started with your website we've made sure to include 4 homepage layouts, 10 unique heros, and some 50+ content blocks that you can use to tailor MID to your needs and preferences.</p>
                                        </div>
										<p><strong>Tags: </strong>
one-column, flexible-header, accessibility-ready, custom-colors, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, rtl-language-support, sticky-post, threaded-comments, translation-ready</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-3 col-md-2 p_plugin_pic">
                                    <div class="price1">
                                        <span class="currency">USD</span>
                                        <!-- <span class="price-number">$<span>--</span> -->
										<div class="alert alert-danger">
					<h3>Coming Soon</h3>
				</div>
										</span>
                                    </div>
                                    <div class="btn-group-vertical">
                                        <a class="btn btn-primary btn-lg" href="#">Detail</a>
                                    </div>
                                </div>
                            </div>
							
							
							<div class="row p_plugin blog_gallery">
                                <div class="col-xs-12 col-sm-4 col-md-4 p_plugin_pic">
                                    <div class="img-thumbnail pro_theme">
										<img src="<?php echo get_template_directory_uri(); ?>/screenshot.png" class="img-fluid" alt="img"/>
										<div class="btn-vertical">
										<h4 class="pro_thm">
                                        <a href="#">Construction Premium</a></h4>
                                        <div class="alert alert-danger">
					<h3>Coming Soon</h3>
				</div>
										</div>
                                    </div>
									
                                </div>
                                
                               
                            </div>
							
							
							<div class="row p_plugin blog_gallery visit_pro">
                                <p>Visit Our Latest Pro Themes & See Demos</p>
								<p style="font-size: 17px !important;">We have put in a lot of effort in all our themes, free and premium both. Each of our Premium themes has a corresponding free version so that you can try out the theme before you decide to purchase it.</p>
								<a href="#">Visit Themes</a>
                            </div>	
                        </div>
                    </div>
					
					<div id="freepro" class="tab-pane fade">
							<div class=" p_head theme">
                                <h1 class="section-title">MI Dexigner Offers</h1>
                            </div>
						<div class="row p_plugin blog_gallery">		
						<div class="col-auto">
						  <ul class="list-group">
							<li class="list-group-item active"><?php echo $themename; ?></li>
							<li class="grey list-group-item"><h1 class="card-title pricing-card-title">Price</h1></li>
							<li  class="list-group-item">Front Page</li>
							<li  class="list-group-item">Parallax Design</li>
							<li class="list-group-item">Theme Option Panel</li>
							<li class="list-group-item">Unlimited Color Skins</li>
							<li class="list-group-item"> Mega Menu Support</li>
							<li class="list-group-item">Multilingual</li>
							<li class="list-group-item">10 Page Layout</li>
							<li class="list-group-item">Contact Page Template</li>
							<li class="list-group-item">About Us Page Template</li>
							<li class="list-group-item">Custom Shortcodes</li>
						  </ul>
						</div>
						
						 <div class="col-auto">
						  <ul class="list-group">
							<li class="list-group-item">Free</li>
							<li class="grey list-group-item "><h1 class="card-title pricing-card-title">$ 00 </h1></li>
							<li class="list-group-item"><i class="fa fa-check"></i></li>
							<li class="list-group-item"><i class="fa fa-times-circle"></i></li>
							<li class="list-group-item"><i class="fa fa-check"></i></li>
							<li class="list-group-item"><i class="fa fa-times-circle"></i></li>
							<li class="list-group-item"><i class="fa fa-times-circle"></i></li>
							<li class="list-group-item"><i class="fa fa-times-circle"></i></li>
							<li class="list-group-item"><i class="fa fa-times-circle"></i></li>
							<li class="list-group-item"><i class="fa fa-check"></i></li>
							<li class="list-group-item"><i class="fa fa-times-circle"></i></li>
							<li class="list-group-item"><i class="fa fa-times-circle"></i></li>
						  </ul>
						</div>

						<div class="columns">
						  <ul class="price">
							<li class="list-group-item active"><?php echo $themename; ?> Pro</li>
							<li class="list-group-item grey"><h1 class="card-title pricing-card-title">$ ---- </h1></li>
							<li class="list-group-item"><i class="fa fa-check"></i></li>
							<li class="list-group-item"><i class="fa fa-check"></i></li>
							<li class="list-group-item"><i class="fa fa-check"></i></li>
							<li class="list-group-item"><i class="fa fa-check"></i></li>
							<li class="list-group-item"><i class="fa fa-check"></i></li>
							<li class="list-group-item"><i class="fa fa-check"></i></li>
							<li class="list-group-item"><i class="fa fa-check"></i></li>
							<li class="list-group-item"><i class="fa fa-check"></i></li>
							<li class="list-group-item"><i class="fa fa-check"></i></li>
							<li class="list-group-item"><i class="fa fa-check"></i></li>
							<li class="grey"><a href="#" class="pro_btn"><h6>Coming Soon</h6></a></li>
						  </ul>
						</div>
						</div>
					</div>
							
                </div>
            </div>            
<?php
	}
}
?>