<?php if (!function_exists('theme_hire_page')) {
	function theme_hire_page() {
	$page1=add_theme_page(__('Welcome to MI Dexigner', 'mid'), __('<span style="color: #ffba00;">Hire Us</span>', 'mid'), 'edit_theme_options', 'mi-hire-page', 'theme_display_theme_info_page');
	
	add_action('admin_print_styles-'.$page1, 'theme_admin_info');
	}	
}
add_action('admin_menu', 'theme_hire_page');

function theme_admin_info(){
	// CSS
	wp_enqueue_style('bootstrap',  get_stylesheet_directory_uri() .'/assets/admin/plugin.css');
	//JS
	wp_enqueue_script('jquery');
	wp_enqueue_script('skypeJs','http://download.skype.com/share/skypebuttons/js/skypeCheck.js');
} 
if (!function_exists('theme_display_theme_info_page')) {
	function theme_display_theme_info_page() {
		$theme_data = wp_get_theme(); ?>
		<style>
.card-cost {
    margin-bottom: 1rem;
}
.card > * {
    text-align: center;
}
.card-value {
    font-weight: 700;
    font-size: 1.6rem;
}
.card-hire {
    font-size: 0.8rem;
}
.card-top {
    height: 70px;
    width: 100%;
    background: #0099d3;
    color: white;
    font-weight: 600;
    border-top-left-radius: 0.3rem;
    border-top-right-radius: 0.3rem;
    display: flex;
    justify-content: center;
    align-items: center;
}
		</style>
		<div class="container">
		<div class="row theme-heighlights">
			<div class="support-data">
				<h4 class="high-title">Get Things Done Quickly:</h4>
				<ul class="list-inline">
					<li class="list-inline-item"><i class="fa fa-star theme-icon"></i> Hire once, fully WP customization.</li>	 	
					<li class="list-inline-item"><i class="fa fa-star theme-icon"></i> Quick support, Quick solution on call or skype</li>					
					<li class="list-inline-item"><i class="fa fa-star theme-icon"></i> Dedicated expert</li>
				</ul>
			</div>
			
		</div>

		<div class="row">

			<div class="card-group">
  <div class="card" style="width: 22rem;padding: 0;border: 0px;">
    <div class="card-top">Starter Plan</div>
    <div class="card-body" style="border: 1px solid rgba(0,0,0,.125);">
      <div class="card-cost">
				<div class="card-value">24$</div>
				<div class="card-hire">Hire me for 04 Hr</div>
			</div>
         <ul class="list-inline">
<li class="list-inline-item"> <a href="callto://+4083843319"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/admin/img/icon_phone.png"  width="42"></a></li>	
<li class="list-inline-item"> <a href="skype:midexigner;idrees290?call"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/admin/img/icon_skype.png"  width="42"></a></li>	
	</ul>
    </div>
  </div>
  <div class="card" style="width: 22rem;padding: 0 20px;border: 0px;">
<div class="card-top">Economy Plan</div>
    <div class="card-body"  style="border: 1px solid rgba(0,0,0,.125);">
             <div class="card-cost">
				<div class="card-value">95$</div>
				<div class="card-hire">Hire me for 08 Hr</div>
			</div>
         <ul class="list-inline">
<li class="list-inline-item"> <a href="callto://+4083843319"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/admin/img/icon_phone.png"  width="42"></a></li>	
<li class="list-inline-item"> <a href="skype:midexigner;idrees290?call"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/admin/img/icon_skype.png"  width="42"></a></li>	
	</ul>
    </div>
  </div>
  <div class="card" style="width: 22rem;padding: 0;border: 0px;">
  	<div class="card-top">Valuable Plan</div>
    <div class="card-body" style="border: 1px solid rgba(0,0,0,.125);">
<div class="card-cost">
				<div class="card-value">500$</div>
				<div class="card-hire">Hire me for 01 Project</div>
			</div>
<ul class="list-inline">
<li class="list-inline-item"> <a href="callto://+4083843319"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/admin/img/icon_phone.png"  width="42"></a></li>	
<li class="list-inline-item"> <a href="skype:midexigner;idrees290?call"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/admin/img/icon_skype.png"  width="42"></a></li>	
	</ul>

	<!-- 
 <a href="skype:midexigner;idrees290?chat">Chat</a> -->
  
  </div>
</div>
		
		</div>
		</div>
<?php
	}
}
?>