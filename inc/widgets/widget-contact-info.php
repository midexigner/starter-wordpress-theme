<?php

class mi_Widget_Contact_Info extends WP_Widget {

/**
	 * Specifies the widget name, description, class name and instatiates it
	 */
	public function __construct() {
		parent::__construct(
			'widget-contact-info',
			__( 'MI: Contact Info ', 'mid' ),
			array(
				'classname'   => 'widget-contact-info',
				'description' => __( 'A  widget that displays Contact Information.', 'mid' )
			)
		);
	}


	/**
	 * Generates the back-end layout for the widget
	 */
	public function form( $instance ) {
		// Default widget settings
		$defaults = array(
			'phone_no'   => '+1 555 123 4567',
			'fax_no'   => '+1 444 123 4567',
			'email' => 'email@domain.com',
			'address'      => '1133  Pinewood Drive',
		);

		$instance = wp_parse_args( (array) $instance, $defaults );

		// The widget content ?>
		<!-- Phone No -->
		<p>
			<label for="<?php echo $this->get_field_id( 'phone_no' ); ?>"><?php _e( 'Phone No:', 'mid' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'phone_no' ); ?>" name="<?php echo $this->get_field_name( 'phone_no' ); ?>" value="<?php echo esc_attr( $instance['phone_no'] ); ?>">
		</p>

		<!-- Fax No -->
		<p>
			<label for="<?php echo $this->get_field_id( 'fax_no' ); ?>"><?php _e( 'Fax No:', 'mid' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'fax_no' ); ?>" name="<?php echo $this->get_field_name( 'fax_no' ); ?>" value="<?php echo esc_attr( $instance['fax_no'] ); ?>">

		</p>

	<!-- Email -->
		<p>
			<label for="<?php echo $this->get_field_id( 'email' ); ?>"><?php _e( 'Email:', 'mid' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'email' ); ?>" name="<?php echo $this->get_field_name( 'email' ); ?>" value="<?php echo esc_attr( $instance['email'] ); ?>">

		</p>

		<!-- Address -->
		<p>
			<label for="<?php echo $this->get_field_id( 'address' ); ?>"><?php _e( 'Address:', 'mid' ); ?></label>
			<textarea  class="widefat" id="<?php echo $this->get_field_id( 'address' ); ?>" name="<?php echo $this->get_field_name( 'address' ); ?>"><?php echo esc_attr( $instance['address'] ); ?></textarea>

		</p> <?php
	}

	/**
	 * Processes the widget's values
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		// Update values
		$instance['phone_no']  = strip_tags( stripslashes( $new_instance['phone_no'] ) );
		$instance['fax_no']   = strip_tags( stripslashes( $new_instance['fax_no'] ) );
		$instance['email'] = strip_tags( stripslashes( $new_instance['email'] ) );
		$instance['address']      = strip_tags( stripslashes( $new_instance['address'] ) );
		return $instance;
	}


	/**
	 * Output the contents of the widget
	 */
	public function widget( $args, $instance ) {
		// Extract the arguments
		extract( $args );

		$phone_no               = $instance['phone_no'];
		$fax_no   = $instance['fax_no'];
		$email = $instance['email'];
		$address      = $instance['address'];

		// Display the markup before the widget (as defined in functions.php)
		echo $before_widget;


		echo '<ul class="list-inline">';

		if ( $phone_no ) : ?>
			<li class="list-inline-item">
			<a href="tel:<?php echo $phone_no; ?>">
				<i class="fa fa-phone"></i>
				<?php echo $phone_no; ?>
			</a>
			</li>
		<?php endif;

		if ( $fax_no ) : ?>
			<li class="list-inline-item">
				<a href="tel:<?php echo $fax_no; ?>">
				<i class="fa fa-fax"></i>
				<?php echo $fax_no; ?>
			</a>
			</li>
		<?php endif;

		if ( $email ) : ?>
			<li class="list-inline-item">
				<a href="tel:<?php echo $email; ?>">
				<i class="fa fa-envelope"></i>
				<?php echo $email; ?>
			</a>
			</li>
		<?php endif;

		if ( $address ) : ?>
			<li class="list-inline-item">
			<i class="fa fa-map-marker"></i>
				<?php echo $address; ?>
			</li>
		<?php endif;

		echo '</ul>';

		// Display the markup after the widget (as defined in functions.php)
		echo $after_widget;
	}


}

// Register the widget using an annonymous function
function register_contact_info_widgets() {
	register_widget( 'mi_Widget_Contact_Info' );
}
add_action( 'widgets_init', 'register_contact_info_widgets' );
 ?>