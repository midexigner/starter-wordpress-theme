<?php
/*
@package mitheme
	
	========================
		LOAD FUNCTION AJAXS
	========================
*/

function mi_check_paged($num = null)
{
    $output = '';
    if (is_paged()) {
        $output = 'page/'.get_query_var('paged');
    }
    if ($num == 1) {
        $paged = (get_query_var('paged') == 0 ? 1 : get_query_var('paged'));
        return $paged;
    } else {
        return $output;
    }
}

add_action( 'wp_ajax_nopriv_mi_load_more', 'mi_load_more' );
add_action( 'wp_ajax_mi_load_more', 'mi_load_more' );

function mi_load_more(){
	
	$paged = $_POST['page']+1;
	
	$query = new wp_Query(array(
		'post_type' => 'post',
		'post_status'=>'publish',
		'paged' => $paged
	));
if ($query->have_posts()) :
 while ($query->have_posts()) :$query->the_post();
 get_template_part('template-parts/post/content',get_post_format()); 
 	endwhile;
 endif;
 wp_reset_postdata();
	die(0);
}