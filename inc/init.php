<?php 
/**
 * init.php
 *
 * Load the widget file
 * Package mid Theme
 * Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright (c) 2019, MI Dexigner (TM)
* Link http://www.midexigner.com
 */
 ?>
<?php  
require FRAMEWORK . '/widgets/widget-business-hours.php';
require FRAMEWORK . '/widgets/widget-contact-info.php';
require FRAMEWORK . '/nav_menu/mi-menu.php';
require FRAMEWORK . 'template-tags.php';
require FRAMEWORK . 'widget.php';
require FRAMEWORK . 'category-image.php';
require FRAMEWORK . 'woocomerce.php';
require FRAMEWORK . 'hooks-functions.php';

?>
