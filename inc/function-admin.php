<?php
/*
	
@package mitheme
	
	========================
		ADMIN PAGE
	========================
*/
if (is_admin()) {
	include dirname( __FILE__ ) .'/admin/hire-us.php';
}
if (is_admin()) {
	include dirname( __FILE__ ) .'/admin/admin-themes.php';
}
function mi_add_admin_page() {
	//Generate Theme Admin Page
	add_theme_page( wp_get_theme().' Options', wp_get_theme(), 'manage_options', 'mi_theme', 'sunset_theme_create_page' );
	
	#add_menu_page( wp_get_theme().' Options', wp_get_theme(), 'manage_options', 'mi_theme', 'sunset_theme_create_page','dashicons-admin-customizer', 110 );

	//Generate Theme Admin Sub Pages
/*	add_submenu_page( 'mi_theme', wp_get_theme().' General Options', 'General', 'manage_options', 'mi_theme', 'sunset_theme_create_page' );

	add_submenu_page( 'mi_theme', wp_get_theme().' Options', 'Maintenance Mode ', 'manage_options', 'mi_sunset_theme', 'sunset_theme_support_page' );

	add_submenu_page( 'mi_theme', wp_get_theme().' Contact Form', 'Contact Form', 'manage_options', 'mi_sunset_theme_contact', 'sunset_contact_form_page' );

	add_submenu_page( 'mi_theme', wp_get_theme().' Miscellaneous', 'Miscellaneous', 'manage_options', 'mi_theme_miscellaneous', 'mi_miscellaneous_page' );

	add_submenu_page( 'mi_theme', wp_get_theme().' Fonts', 'Fonts', 'manage_options', 'mi_theme_fonts', 'mi_fonts_page' );
	if(!empty(get_option( 'maintenance_enabled' ))){
	add_submenu_page( 'mi_theme', wp_get_theme().' Coming Soon', 'Coming Soon', 'manage_options', 'mi_theme_comingsoon', 'mi_ComingSoon_page' );
	}
	add_submenu_page( 'mi_theme', wp_get_theme().' CSS Options', 'Custom CSS', 'manage_options', 'mi_sunset_css', 'sunset_theme_settings_page');*/

// Activate custom settings

add_action( 'admin_init','mi_custom_settings');
	
}

add_action( 'admin_menu', 'mi_add_admin_page');

function mi_custom_settings(){

// register Settings
include dirname( __FILE__ ) .'/templates/mi-register-settings.php';

// register Section
include dirname( __FILE__ ) .'/templates/mi-settings-section.php';

// Add settings field
include dirname( __FILE__ ) .'/templates/mi-settings-field.php';

	
	
	
	

}
function sunset_custom_css_section_callback() {
	echo 'Customize Sunset Theme with your own CSS';
}

function sunset_sanitize_custom_css( $input ){
	$output = esc_textarea( $input );
	return $output;
}

function mi_sanitize_custom_font( $input ){
	var_dump($input);
	//$output = esc_textarea( $input );
	//return $output;
}

function sunset_custom_css_callback() {
	$css = get_option( 'sunset_css' );
	$css = ( empty($css) ? '/* '.wp_get_theme().' Custom CSS */' : $css );
	echo '<div id="customCss">'.$css.'</div><textarea id="sunset_css" name="sunset_css" style="display:none;visibility:hidden;">'.$css.'</textarea>';
}

 // Post Formats callback Function
function mi_post_formats_callback($input){
	return $input;
}
function mi_fonts_formats_callback($input){
	return $input;
}  

function sunset_theme_options() {
	
}

function sunset_contact_section() {
	echo 'Activate and Deactivate the Built-in Contact Form';
}
function mi_misce_section() {
	
}

function mi_theme_fontsName() {
	echo 'Please Select  Specific Fonts Family Options';
}

function mi_theme_ComingSoon() {
	echo 'Please  Customization Coming Soon';
}

function sunset_activate_contact() {
	$options = get_option( 'activate_contact' );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="custom_header" name="activate_contact" value="1" '.$checked.' /></label>';
}

function mi_widget_enabled() {
	$options = get_option( 'widget_enabled' );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="widget_enabled" name="widget_enabled" value="1" '.$checked.' />Activate the Widget Area</label>';
}

function mi_loader_enabled() {
	$options = get_option( 'loader_enabled' );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="loader_enabled" name="loader_enabled" value="1" '.$checked.' /> Activate the Loader</label>';
}
function mi_scroller_enabled() {
	$options = get_option( 'scroller_enabled' );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="scroller_enabled" name="scroller_enabled" value="1" '.$checked.' /> Activate the Scroller</label>';
}
function mi_maintenance_enabled() {
	$options = get_option( 'maintenance_enabled' );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="maintenance_enabled" name="maintenance_enabled" value="1" '.$checked.' /> Enable Coming Soon</label>';
}
function mi_backtotop_enabled() {
	$options = get_option( 'backtotop_enabled' );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="backtotop_enabled" name="backtotop_enabled" value="1" '.$checked.' /> Activate the Back to top Pages</label>';
}
function mi_updateclose_enabled() {
	$options = get_option( 'updateclose_enabled' );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="updateclose_enabled" name="updateclose_enabled" value="1" '.$checked.' /> Update Closed Word Press</label>';
}

function sunset_post_formats() {
	$options = get_option( 'post_formats' );
	$formats = array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' );
	$output = '';
	foreach ( $formats as $format ){
		$checked = ( @$options[$format] == 1 ? 'checked' : '' );
		$output .= '<label><input type="checkbox" id="'.$format.'" name="post_formats['.$format.']" value="1" '.$checked.' /> '.$format.'</label><br>';
	}
	echo $output;
}
function sunset_custom_header() {
	$options = get_option( 'custom_header' );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="custom_header" name="custom_header" value="1" '.$checked.' /> Activate the Custom Header</label>';
}
function sunset_custom_background() {
	$options = get_option( 'custom_background' );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="custom_background" name="custom_background" value="1" '.$checked.' /> Activate the Custom Background</label>';
}


// Sidebar Options Functions
function sunset_sidebar_options() {
	echo 'Customize your Information';
}

function sunset_sidebar_profile(){
	$picture = esc_attr( get_option( 'upload_logo' ) );
	if(empty($picture)){
echo '<input type="button" class="button button-secondary" value="Upload Logo" id="upload-button"><input type="hidden" id="add-logo" class="regular-text" name="upload_logo" value=""/>';
	}else{
		echo '<img src="'.$picture.'" width="80"/><br/>';
		echo '<input type="button" class="button button-secondary" value="Upload Logo" id="upload-button"><input type="hidden" id="add-logo class="regular-text" name="upload_logo" value="'.$picture.'"/> <input type="button" id="remove-logo" class="button button-secondary" name="upload_logo" value="Remove Logo">';
	}
}

function sunset_sidebar_name() {
	$emailAddress = esc_attr( get_option( 'email_address' ) );
	$phoneNumber = esc_attr( get_option( 'phone_number' ) );
	$address = esc_attr( get_option( 'address' ) );
	$website = esc_attr( get_option( 'website' ) );
	echo '<label class="label">Email Address</label><input type="text" class="regular-text" name="email_address" value="'.$emailAddress.'" placeholder="Email Address" />';
	echo '<label class="label">Phone Number</label><input type="text" class="regular-text" name="phone_number" value="'.$phoneNumber.'" placeholder="Phone No" /><br/>';
	echo '<label class="label"> Address</label><input type="text" class="regular-text" name="address" value="'.$address.'" placeholder="Address" />';
	echo '<label class="label">Website</label><input type="text" class="regular-text" name="website" value="'.$website.'" placeholder="Website" />';
}
function sunset_sidebar_description() {
	$copyRight = esc_attr( get_option( 'copyright_text' ) );
	
	echo '<input type="text" class="regular-text" name="copyright_text" value="'.$copyRight.'" placeholder="Copyright Text" />';
}

function sunset_sidebar_twitter(){
	$twitter = esc_attr( get_option( 'twitter_handler' ) );
	echo '<input type="text" class="regular-text" name="twitter_handler" value="'.$twitter.'" placeholder="Twitter handler" /><p class="description">Input Twitter username without the @ character.</p>';
}
function sunset_sidebar_facebook(){
	$facebook = esc_attr( get_option( 'facebook_handler' ) );
	echo '<input type="text" class="regular-text" name="facebook_handler" value="'.$facebook.'" placeholder="Facebook Handler" /><p class="description">Input Facebook username add.</p>';
}

function sunset_sidebar_gplus(){
	$gplus = esc_attr( get_option( 'gplus_handler' ) );
	echo '<input type="text" class="regular-text" name="gplus_handler" value="'.$gplus.'" placeholder="Google+ Handler" /><p class="description">Input Google+ username add.</p>';
}

// Sanitization setting
function sunset_sanitize_twitter_handler($input){
$output = sanitize_text_field( $input );
$output = str_replace('@', '', $output);
return $output;
}

function mi_theme_fonts_body() {
	
	$options = esc_attr(get_option( 'miFontsBody' ));
	echo '<select id="" class="fontName-list large-text" type="text" name="miFontsBody" data-value="'.$options.'">
	<option>Please Select Body Font</option>
						    </select>';
}

function mi_theme_fonts_heading() {
	
	$options = esc_attr(get_option( 'miFontsHeading' ));
	echo '<select id="" class="fontName-list large-text" type="text" name="miFontsHeading" data-value="'.$options.'">
	<option>Please Select all Heading Font</option>
						    </select>';
}
function mi_theme_anyalytics_api() {
	$options = esc_attr(get_option( 'miGoogleAnyalyticsApi' ));
	echo '<textarea name="miGoogleAnyalyticsApi" rows="10" cols="10" class="large-text code">'.$options.'</textarea><p class="description">Write something smart.</p>';
}

function mi_theme_comming_soon() {
	$CommingSoonHeading = esc_attr(get_option( 'CommingSoonHeading' ));
	$CommingSoonText = esc_attr(get_option( 'CommingSoonText' ));
	$CommingSoonYear = esc_attr(get_option( 'CommingSoonYear' ));
	$CommingSoonMonth = esc_attr(get_option( 'CommingSoonMonth' ));
	$CommingSoonDay = esc_attr(get_option( 'CommingSoonDay' ));
	echo '<label class="label">Heading</label><br/>';
	echo '<input type="text" class="regular-text" name="CommingSoonHeading" value="'.$CommingSoonHeading.'" placeholder="Coming Soon" /><p class="description">Main Title Heading</p>';

	echo '<label class="label">Text</label><br/>';
	echo '<textarea name="CommingSoonText" placeholder="Our website is under construction."  rows="10" cols="10" class="large-text code">'.$CommingSoonText.'</textarea><p class="description">Write something smart.</p>';

	echo '<label class="label">Year</label><br/>';
	echo '<input type="text" class="regular-text" name="CommingSoonYear" value="'.$CommingSoonYear.'" placeholder="2019" /><p class="description">Enter Year (for example 2019)</p>';
	echo '<label class="label">Month</label><br/>';
	echo '<input type="text" class="regular-text" name="CommingSoonMonth" value="'.$CommingSoonMonth.'" placeholder="Month February" /><p class="description">Enter Month (for example February)</p>';

	echo '<label class="label">Day</label><br/>';
	echo '<input type="text" class="regular-text" name="CommingSoonDay" value="'.$CommingSoonDay.'" placeholder="Day 01" /><p class="description">Enter Day  (for example 01)</p>';
}




// Template submenu function

function sunset_theme_create_page(){
	//generation of our admin page
	include dirname( __FILE__ ) .'/templates/mi-admin.php';

}



function sunset_theme_support_page(){
	//generation of our admin page
	
	include dirname( __FILE__ ) .'/templates/mi-theme-support.php';
}

function sunset_contact_form_page() {
	include dirname( __FILE__ ) .  '/templates/mi-contact-form.php' ;
}

function mi_miscellaneous_page() {
	include dirname( __FILE__ ) . '/templates/mi-miscellaneous.php' ;
}

function mi_fonts_page() {
	include dirname( __FILE__ ) . '/templates/mi-fonts.php';
}

function mi_ComingSoon_page() {
	include dirname( __FILE__ ) . '/templates/mi-coming-soon.php';
}

// Template submenu function
function sunset_theme_settings_page(){
	//generation of our admin page
	include dirname( __FILE__ ) . '/templates/mi-custom-css.php';
}

