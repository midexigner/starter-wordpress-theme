<h1><?php echo wp_get_theme(); ?></h1>
<!-- <h3 class="title">Manage Options</h3>
<p>Customize Sidebar Options</p> -->
<?php settings_errors(); ?>
<?php 
	$emailAddress = esc_attr( get_option( 'email_address' ) );
	$phoneNumber = esc_attr( get_option( 'phone_number' ) );
	$address = esc_attr( get_option( 'address' ) );
	$website = esc_attr( get_option( 'website' ) );
	$picture = esc_attr( get_option( 'upload_logo' ) );
	$copyRight = esc_attr( get_option( 'copyright_text' ) );
 ?>
<div class="mi-sidebar-preview">
	<div class="mi-sidebar">
		<h1 class="image-container">
			<div id="add-logo-preview" class="logo-preview" style="background-image: url('<?php print $picture; ?>')">
			</div>
		</h1>
		<h3 class="mi-emailAddress"><?php print $emailAddress; ?></h3>
		<h3 class="mi-phoneNumber"><?php print $phoneNumber; ?></h3>
		<h3 class="mi-address"><?php print $address; ?></h3>
		<h3 class="mi-website"><?php print $website; ?></h3>
		<h3 class="mi-copyRight"><?php print $copyRight; ?></h3>
		<div class="icons-wrapper">
			
		</div>
	</div>
</div>
<form method="post" action="options.php" class="mi-form">
	<?php settings_fields( 'sunset-settings-group' ); ?>
	<?php do_settings_sections( 'mi_sunset' ); ?>
	<?php submit_button('Save Changes','primary','btnSubmit'); ?>
</form>