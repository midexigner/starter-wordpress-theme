<h1><?php echo wp_get_theme(); ?> Fonts</h1>
<?php settings_errors(); ?>
<form method="post" action="options.php" class="mi-form">
	<?php settings_fields( 'mi-fonts-options' ); ?>
	<?php do_settings_sections( 'mi_theme_fonts' ); ?>
	<?php submit_button('Save Changes','primary','btnSubmit'); ?>
</form>