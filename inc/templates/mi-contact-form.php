<h1><?php echo wp_get_theme(); ?> Contact Form</h1>
<?php settings_errors(); ?>

<form method="post" action="options.php" class="mi-form">
	<?php settings_fields( 'sunset-contact-options' ); ?>
	<?php do_settings_sections( 'mi_sunset_theme_contact' ); ?>
	<?php submit_button(); ?>
</form>