<h1><?php echo wp_get_theme(); ?> Customize Coming Soon</h1>
<?php settings_errors(); ?>
<form method="post" action="options.php" class="mi-coming-soon">
	<?php settings_fields( 'mi-commingsoon-options' ); ?>
	<?php do_settings_sections( 'mi_theme_comingsoon' ); ?>
	<?php submit_button('Save Changes','primary','btnSubmit'); ?>
</form>