<?php 
// General Option
add_settings_field( 'sidebar-profile-picture', 'Logo', 'sunset_sidebar_profile', 'mi_sunset', 'sunset-sidebar-options');

add_settings_field( 'sidebar-name', 'Contact Information', 'sunset_sidebar_name', 'mi_sunset', 'sunset-sidebar-options');

add_settings_field( 'sidebar-description', 'Copyright Text', 'sunset_sidebar_description', 'mi_sunset', 'sunset-sidebar-options');

add_settings_field( 'sidebar-twitter', 'Twitter handler', 'sunset_sidebar_twitter', 'mi_sunset', 'sunset-sidebar-options');

add_settings_field( 'sidebar-facebook', 'Facebook handler', 'sunset_sidebar_facebook', 'mi_sunset', 'sunset-sidebar-options');

add_settings_field( 'sidebar-gplus', 'Google+ handler', 'sunset_sidebar_gplus', 'mi_sunset', 'sunset-sidebar-options');

// Activate Option
add_settings_field( 'post-formats', 'Post Formats', 'sunset_post_formats', 'mi_sunset_theme', 'sunset-theme-options' );

add_settings_field( 'custom-header', 'Custom Header', 'sunset_custom_header', 'mi_sunset_theme', 'sunset-theme-options' );

add_settings_field( 'custom-background', 'Custom Background', 'sunset_custom_background', 'mi_sunset_theme', 'sunset-theme-options' );

add_settings_field( 'widget-enabled', 'Widget Enabled', 'mi_widget_enabled', 'mi_sunset_theme', 'sunset-theme-options' );

add_settings_field( 'loader-enabled', 'Loader Enabled', 'mi_loader_enabled', 'mi_sunset_theme', 'sunset-theme-options' );
add_settings_field( 'scroller-enabled', 'Scroller Enabled', 'mi_scroller_enabled', 'mi_sunset_theme', 'sunset-theme-options' );

add_settings_field( 'maintenance-enabled', 'Maintenance Enabled', 'mi_maintenance_enabled', 'mi_sunset_theme', 'sunset-theme-options' );

add_settings_field( 'backtotop-enabled', 'Back to top Enabled', 'mi_backtotop_enabled', 'mi_sunset_theme', 'sunset-theme-options' );
add_settings_field( 'updateclose-enabled', 'Update Close Wordpress', 'mi_updateclose_enabled', 'mi_sunset_theme', 'sunset-theme-options' );

// Contact Form
add_settings_field( 'activate-form', 'Activate Contact Form', 'sunset_activate_contact', 'mi_sunset_theme_contact', 'sunset-contact-section' );

// Custom CSS
add_settings_field( 'custom-css', 'Insert your Custom CSS', 'sunset_custom_css_callback', 'mi_sunset_css', 'sunset-custom-css-section' );

// Typographic Option
add_settings_field( 'mi-font-body', 'Body', 'mi_theme_fonts_body','mi_theme_fonts', 'mi-fonts-section');
add_settings_field( 'mi-font-heading', 'Heading', 'mi_theme_fonts_heading','mi_theme_fonts', 'mi-fonts-section');


add_settings_field( 'mi-anyalytics-api', 'Google Analytic API', 'mi_theme_anyalytics_api','mi_theme_miscellaneous', 'mi-miscellaneous-section');

add_settings_field( 'mi-commingsoon-id', '', 'mi_theme_comming_soon','mi_theme_comingsoon', 'mi-commingsoon-section');

//add_settings_field( $id, $title, $callback, $page, $section = 'default', $args = array );
 ?>