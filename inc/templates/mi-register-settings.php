<?php 
// $option_group, $option_name, args = array()

	// General Option
	register_setting( 'sunset-settings-group', 'upload_logo' );
	register_setting( 'sunset-settings-group', 'email_address' );
	register_setting( 'sunset-settings-group', 'phone_number' );
	register_setting( 'sunset-settings-group', 'address' );
	register_setting( 'sunset-settings-group', 'website' );
	register_setting( 'sunset-settings-group', 'copyright_text' );
	register_setting( 'sunset-settings-group', 'twitter_handler', 'sunset_sanitize_twitter_handler');
	register_setting( 'sunset-settings-group', 'facebook_handler' );
	register_setting( 'sunset-settings-group', 'gplus_handler' );


	// Theme Option

	register_setting( 'sunset-theme-support', 'post_formats','mi_post_formats_callback' );
	register_setting( 'sunset-theme-support', 'custom_header' );
	register_setting( 'sunset-theme-support', 'widget_enabled' );
	register_setting( 'sunset-theme-support', 'custom_background' );
	register_setting( 'sunset-theme-support', 'loader_enabled' );
	register_setting( 'sunset-theme-support', 'scroller_enabled' );
	register_setting( 'sunset-theme-support', 'maintenance_enabled' );
	register_setting( 'sunset-theme-support', 'backtotop_enabled' );
	register_setting( 'sunset-theme-support', 'updateclose_enabled' );


	//Contact Form Options
	register_setting( 'sunset-contact-options', 'activate_contact' );

	// Custom CSS Options
	register_setting( 'sunset-custom-css-options', 'sunset_css', 'sunset_sanitize_custom_css' );

// Fonts Typo group
	register_setting( 'mi-fonts-options', 'miFontsBody');
	register_setting( 'mi-fonts-options', 'miFontsHeading');


// coming soon group
	register_setting( 'mi-commingsoon-options', 'CommingSoonHeading');
	register_setting( 'mi-commingsoon-options', 'CommingSoonText');
	register_setting( 'mi-commingsoon-options', 'CommingSoonYear');
	register_setting( 'mi-commingsoon-options', 'CommingSoonMonth');
	register_setting( 'mi-commingsoon-options', 'CommingSoonDay');

// mi-miscellaneous-section
	register_setting( 'mi-miscellaneous-options', 'miGoogleAnyalyticsApi');


 ?>