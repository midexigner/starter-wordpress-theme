<h1  class="title"><?php echo wp_get_theme(); ?> Theme Support</h1>
<?php settings_errors(); ?>
<form method="post" action="options.php" class="mi-form">
	<?php settings_fields( 'sunset-theme-support' ); ?>
	<?php do_settings_sections( 'mi_sunset_theme' ); ?>
	<?php submit_button(); ?>
</form>