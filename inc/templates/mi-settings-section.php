<?php 
// General Option
add_settings_section( 'sunset-sidebar-options', 'General Option', 'sunset_sidebar_options', 'mi_sunset' );

// Theme Option
add_settings_section( 'sunset-theme-options', 'Theme Options', 'sunset_theme_options', 'mi_sunset_theme' );
 //Contact Form Options
 add_settings_section( 'sunset-contact-section', 'Contact Form', 'sunset_contact_section', 'mi_sunset_theme_contact');

// Miscellaneous Options
add_settings_section( 'mi-miscellaneous-section', 'Miscellaneous List', 'mi_misce_section', 'mi_theme_miscellaneous');

 // Custom CSS Options
 add_settings_section( 'sunset-custom-css-section', 'Custom CSS', 'sunset_custom_css_section_callback', 'mi_sunset_css' );

add_settings_section( 'mi-fonts-section', 'Typography', 'mi_theme_fontsName', 'mi_theme_fonts' );

add_settings_section( 'mi-commingsoon-section', 'Coming Soon', 'mi_theme_ComingSoon', 'mi_theme_comingsoon' );
//add_settings_section( $id, $title, $callback, $page )