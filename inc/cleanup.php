<?php

/*
	
 @package mi_sunset
	
	========================
		REMOVE GENERATOR VERSION NUMBER
	========================
*/

/* remove version string from js and css */
function sunset_remove_wp_version_strings( $src ) {
	
	global $wp_version;
	parse_str( parse_url($src, PHP_URL_QUERY), $query );
	if ( !empty( $query['ver'] ) && $query['ver'] === $wp_version ) {
		$src = remove_query_arg( 'ver', $src );
	}
	return $src;
	
}
add_filter( 'script_loader_src', 'sunset_remove_wp_version_strings' );
add_filter( 'style_loader_src', 'sunset_remove_wp_version_strings' );

/* remove metatag generator from header */
function sunset_remove_meta_version() {
	return '';
}
add_filter( 'the_generator', 'sunset_remove_meta_version' );

/*
//Disable WP Admin Bar Removal
show_admin_bar( false );
if ( ! current_user_can( 'manage_options' ) ) {
    show_admin_bar( false );
}
add_filter('show_admin_bar', '__return_false');



//Hide Front-End Admin Bar Including 32 px Spacing

add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}

 */


$updateClosed = get_option( 'updateclose_enabled' );
if($updateClosed == 1){
function remove_core_updates(){
global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}
add_filter('pre_site_transient_update_core','remove_core_updates');
add_filter('pre_site_transient_update_plugins','remove_core_updates');
add_filter('pre_site_transient_update_themes','remove_core_updates');
}