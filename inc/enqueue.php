<?php
/*
	
@package mitheme
	
	========================
		ADMIN ENQUEUE FUNCTIONS
	========================
*/
function mi_load_admin_scripts( $hook ){
	//echo $hook;
	$setCSs =str_replace(' ', '-',strtolower(wp_get_theme())).'_page_mi_sunset_css';
	$setFonts =str_replace(' ', '-',strtolower(wp_get_theme())).'_page_mi_theme_fonts';
	

	if( 'toplevel_page_mi_theme' == $hook ){

	//register css admin section
	wp_register_style( 'mi_admin', ROOT . 'admin/mi.admin.css', array(), '1.0.0', 'all' );
	wp_enqueue_style( 'mi_admin' );

	wp_enqueue_media();

	//register js admin section
	wp_register_script( 'mi-admin-script',ROOT . 'admin/mi.admin.js' , array('jquery'), '1.0.0', true );

	

	wp_enqueue_script( 'mi-admin-script' );
	


} else if ( $setCSs == $hook ){
	wp_enqueue_style( 'ace', ROOT .  'admin/mi.ace.css', array(), '1.0.0', 'all' );
	wp_enqueue_script( 'ace', ROOT.  'admin/ace/ace.js' , array('jquery'), '1.2.1', true );
	wp_enqueue_script( 'mi-custom-css-script', ROOT  . 'admin/mi.custom_css.js' , array('jquery'), '1.0.0', true );
	
	}
 else if ( $setFonts == $hook ){
 wp_register_script( 'mi-admin-fonts',ROOT . 'admin/mi.fonts.js' , array('jquery'), '1.0.0', true );
wp_enqueue_script( 'mi-admin-fonts' );	
 }
	else{ return; }
	
}
add_action( 'admin_enqueue_scripts', 'mi_load_admin_scripts' );


/*
	
	========================
		FRONT-END ENQUEUE FUNCTIONS
	========================
*/

function mi_load_scripts(){
	
	wp_enqueue_style( 'plugin', STYLES . 'plugin.css', array(), '1.0.0', 'all' );
	#wp_enqueue_style( 'raleway', 'https://fonts.googleapis.com/css?family=Raleway:200,300,500' );
	wp_enqueue_style( 'style', STYLES . 'style.css', array(), '1.0.0', 'all' );
	
	#wp_deregister_script( 'jquery' );
	wp_enqueue_script('jquery-ui-core');
wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
	wp_register_script( 'plugins' , SCRIPTS . 'plugins.js', false, '1.0.0', true );
	wp_enqueue_script( 'plugins' );
	wp_enqueue_script( 'mid', SCRIPTS . 'mi.js', false, '1.0.0', true );
	wp_enqueue_script( 'main', SCRIPTS . 'main.js', false, '1.0.0', true );

	
}
add_action( 'wp_enqueue_scripts', 'mi_load_scripts' );