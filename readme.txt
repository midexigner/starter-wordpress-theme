=== MID ===
Contributors: MI Dexigner team
Tags: one-column, flexible-header, accessibility-ready, custom-colors, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, rtl-language-support, sticky-post, threaded-comments, translation-ready
Requires at least: 4.9.6
Tested up to: WordPress 5.0
Stable tag: 1.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Our 2019 default theme is designed to show off the power of the block editor.

== Description ==
Our 2019 default theme is designed to show off the power of the block editor. It features custom styles for all the default blocks, and is built so that what you see in the editor looks like what you'll see on your website. Twenty Nineteen is designed to be adaptable to a wide range of websites, whether you’re running a photo blog, launching a new business, or supporting a non-profit. Featuring ample whitespace and modern sans-serif headlines paired with classic serif body text, it's built to be beautiful on all screen sizes.

== Changelog ==

= 1.0 =
* Released: December 6, 2018

Initial release

== Resources ==
* jQuery v1.12.4
* Bootstrap v4.0.0
* Waypoints v4.0.1
* Owl Carousel v2.2.1
* animate.css v3.6.0
* Magnific Popup * v1.1.0
* nicescroll v3.7.6 
* Font Awesome Free 5.0.6
* IcoFont v1.0.1
* MeanMenu 2.0.7 
* slick
* jQuery v1.12.4
* Isotope v3.0.5
* jQuery One Page Nav v3.0.0
* WOW v1.1.3
* scrollup v2.4.1
* imagesLoaded v4.1.4
* parallax.js v1.4.2
