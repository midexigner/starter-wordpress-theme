<?php 
$logo = esc_attr( get_option( 'upload_logo' ) ); 
?>

<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand">
	<?php if($logo != ''): ?>
		<img src="<?php echo $logo ?>" alt="<?php echo bloginfo('name'); ?>">
	<?php else: ?>
	<?php echo bloginfo('name'); ?>
	<?php endif; ?>	
	</a>
