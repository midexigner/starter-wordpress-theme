<?php

/**
* content-none.php
* Package mid Theme
* Since 1.0
* Author MI Dexigner : http://www.midexigner.com
* Copyright (c) 2019, MI Dexigner (TM)
* Link http://www.midexigner.com
*/
?>

<div class="not-found">
	<h2><?php _e('Nothing found','mid');?></h2>
</div>