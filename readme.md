starter theme
==================================

# A list of files that need to be written
* functions.php
* index.php
* header.php
* footer.php
* page.php
* single.php
* content.php (with all content types) after create the content all page, import the 
  https://codex.wordpress.org/Theme_Unit_Test
* author archives
* date archives
* category and tag archives
* 404.php and search.php
* full width template
* contact template
* widgets
* css

# Library

- jQuery v1.12.4
- Bootstrap v4.0.0
- Waypoints v4.0.1
- Owl Carousel v2.2.1
- animate.css v3.6.0
- Magnific Popup - v1.1.0
- nicescroll v3.7.6 
- Font Awesome Free 5.0.6
- IcoFont v1.0.1
- MeanMenu 2.0.7 
- slick
- jQuery v1.12.4
- Isotope v3.0.5
- jQuery One Page Nav v3.0.0
- WOW - v1.1.3
- scrollup v2.4.1
- imagesLoaded v4.1.4
- parallax.js v1.4.2
- mail sending function

# installing software required
- rubyinstaller
- sass
- compass sass

# New Project 
compass create <projectName>

# Exiting Project
compass install compass
compass install susy
compass watch

# Resoureces Link
https://bundlephobia.com/
https://www.figma.com/
https://www.2dimensions.com/about-flare
https://fontflipper.com/upload
https://github.com/GoogleChromeLabs/ProjectVisBug
https://insomnia.rest/ (rest api)
https://cloudcraft.co/



https://ez-form-calculator.ezplugins.de/

